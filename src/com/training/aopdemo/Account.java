package com.training.aopdemo;

public class Account {
	

	private String name;
	private String serviceName;
	
	public Account(String name, String serviceName) {
		this.name = name;
		this.serviceName = serviceName;
	}

	public Account() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@Override
	public String toString() {
		return "Account [name=" + name + ", serviceName=" + serviceName + "]";
	}
	
}
