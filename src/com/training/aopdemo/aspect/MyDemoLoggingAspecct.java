package com.training.aopdemo.aspect;

import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.training.aopdemo.Account;
import com.training.aopdemo.dao.AccountDAO;

@Aspect
@Component
public class MyDemoLoggingAspecct {

	@Before("com.training.aopdemo.aspect.SharedAOPUtulities.forAllPackageNoGetterSetter()")
	public void beforeFindAccountsAdvice() {
		System.out.println("==============> @before Find Accounts Advice");
	}

	@AfterReturning(pointcut = "com.training.aopdemo.aspect.SharedAOPUtulities.forAllPackageNoGetterSetter()", returning = "result")
	public void afterReturningFindAccountsAdvice(JoinPoint theJoinPoint, List<Account> result) {
	
		System.out.println("==============> after Returning Find Accounts Advice");
		for (Account account : result) {
			System.out.println(account.toString());
		}
		
		System.out.println("\n Now let's change some data ");
		
		// modifing data 
		if (!result.isEmpty()) {
			Account tempAccount = result.get(0);
			tempAccount.setName("Stupid Daffy");
			
		}
		for (Account account : result) {
			System.out.println(account.toString());
		}
		

	}
}
